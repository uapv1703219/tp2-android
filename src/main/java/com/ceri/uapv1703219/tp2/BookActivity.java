package com.ceri.uapv1703219.tp2;

import android.app.Activity;
import android.app.ActivityGroup;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final Book book =(Book) getIntent().getParcelableExtra("book");
        Boolean estVide = false;
        if(book.getTitle().length() == 0) { estVide = true; }


        final EditText nameBookEdit = findViewById(R.id.nameBook);
        final EditText editAuthor = findViewById(R.id.editAuthor);
        final EditText editYear = findViewById(R.id.editYear);
        final EditText editGenre = findViewById(R.id.editGenre);
        final EditText editPublisher = findViewById(R.id.editPublisher);

        final Button save = findViewById(R.id.button);

        final BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);

        nameBookEdit.setText(book.getTitle());
        editAuthor.setText(book.getAuthors());
        editYear.setText(book.getYear());
        editGenre.setText(book.getGenres());
        editPublisher.setText(book.getPublisher());

        final Boolean finalEstVide = estVide;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    Integer.parseInt(String.valueOf(editYear.getText()));
                    book.setTitle(String.valueOf(nameBookEdit.getText()));
                    book.setAuthors(String.valueOf(editAuthor.getText()));
                    book.setYear(String.valueOf(editYear.getText()));
                    book.setGenres(String.valueOf(editGenre.getText()));
                    book.setPublisher(String.valueOf(editPublisher.getText()));

                    if(!finalEstVide) {bookDbHelper.updateBook(book);
                        Toast.makeText(BookActivity.this, "SAVE", Toast.LENGTH_LONG).show();}
                    else {bookDbHelper.addBook(book);
                        Toast.makeText(BookActivity.this, "ADD", Toast.LENGTH_LONG).show();}

                    finish();
                }
                catch (NumberFormatException e)
                {
                    Toast.makeText(BookActivity.this, "Année mal saisie !!!!!!!!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
